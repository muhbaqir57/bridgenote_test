<?php

namespace Tests\Feature;

use App\Models\employee;
use Tests\TestCase;

class ApiTest extends TestCase
{
    public function testCreateEmployee()
    {
        $data   =   [
                        "user_id"   => "1", 
                        "status"    => "active", 
                        "position"  => "manager",
                    ];

        $this->json(
                        "POST", 'api/employee', 
                        $data, 
                        [
                            'Accept'        => 'application/json',
                            'Authorization' => 'Bearer BSl6tWResWqncBpVuTkNmFll2CisKv2UmuS9Nzw9'
                        ]
                    )
            ->assertStatus(200)
            ->assertJsonStructure([
                    "status"    =>  "success",
                    "data"      =>  "data sucessfuly inserted"
                ]
            );
    }

    public function testRetrieveEmployee()
    {
        $data   =   [
                        "user_id"   => "1",
                    ];

        $this->json(    
                        "GET", 'api/employee/' . $data['user_id'], 
                        [
                            'Accept' => 'application/json',
                            'Authorization' => 'Bearer BSl6tWResWqncBpVuTkNmFll2CisKv2UmuS9Nzw9'
                        ]
                    )
            ->assertStatus(200)
            ->assertJsonStructure([
                    "status"    =>  "success",
                    "data"      =>  "data sucessfuly deleted"
                ]
            );
    }

    public function testUpdateEmployee()
    {
        $data   =   [
                        "user_id"   => "1", 
                        "status"    => "active", 
                        "position"  => "manager"
                    ];

        $this->json(
                        "PATCH", 'api/employee/' . $data['user_id'], 
                        $data, 
                        [
                            'Accept' => 'application/json',
                            'Authorization' => 'Bearer BSl6tWResWqncBpVuTkNmFll2CisKv2UmuS9Nzw9'
                        ]
                    )
            ->assertStatus(200)
            ->assertJsonStructure([
                    "status"    =>  "success",
                    "data"      =>  "data sucessfuly updated"
                ]
            );
    }

    public function testDeleteEmployee()
    {
        $data   =   [
                        "user_id"   => "1", 
                        "status"    => "active", 
                        "position"  => "manager"
                    ];

        $this->json(
                        "DELETE", 'api/employee/' . $data['user_id'], 
                        $data, 
                        [
                            'Accept' => 'application/json',
                            'Authorization' => 'Bearer BSl6tWResWqncBpVuTkNmFll2CisKv2UmuS9Nzw9'
                        ]
                    )
            ->assertStatus(200)
            ->assertJsonStructure([
                    "status"    =>  "success",
                    "data"      =>  "data sucessfuly deleted"
                ]
            );
    }
}

?>