<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{
    public function testRequiredFieldOnLogin()
    {
        $this->json('POST', '/login', ['Accept' => 'application/x-www-form-urlencoded'])
            ->assertStatus(422)
            ->assertJson([
                "message"   => "The given data is invalid",
                "errors"    => [
                        "email"  =>  ["username is required"],
                        "password"  =>  ["password is required"],
                        "_token"    =>  ["Application Token missing"]
                ]
            ]);
    }
}

?>