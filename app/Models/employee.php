<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Model\User;

class employee extends Model
{
    use HasFactory;

    public $timestamps      =   false;
    protected $table        =   'employee';
    protected $primaryKey   =   'user_id';
    

    protected $fillable     = [
        'status',
        'position'
    ];
}
