<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\employee;

class EmployeeAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employee   =   new employee();

        $employee->user_id  =   $request->get('user_id');
        $employee->status   =   $request->get('status');
        $employee->position =   $request->get('position');

        if( $employee->save() )
        {
            return response()->json( ["status" => "success", "data" => "data successfuly inserted"] );
        }

        return response()->json( ["status" => "failed", "data" => "unable to save new employee"] );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json( employee::where( "user_id" ,$id )->first() );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{

            $employee   =   employee::where( "user_id", $id )->first();

            $employee->status   =   $request->get('status');
            $employee->position =   $request->get('position');

            $employee->save();

        } catch(\Exception $e){

            return response()->json( ["response" => $e->getMessage()] );

        }

        return response()->json( ["status" => "success", "data" => "data successfuly updated"] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee   =   employee::where( "user_id", $id )->first();

        if( isset($employee) )
        {
            $employee->delete();
            return response( ["status" => "success", "data" => "data successfuly deleted"] );
        }
    }
}
