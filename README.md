---
## Welcome

This is test for applying position in Bridge Note Indonesia, heres some tips to run this program in your local machine :

### Minimum Requirement (Tested)
```
1. PHP ^v7.4
2. Node ^v12.14.0
3. Composer ^1.6.3

```

---
### Instalation

Clone this repository by running :

```sh
$ git clone https://muhbaqir57@bitbucket.org/muhbaqir57/bridgenote_test.git
```

After clone, setup `.env` by copying `.env.example` and config with your local setup

After finish, run this command to install composer and npm

```sh
$ composer install && npm install
```

Build npm by running :
```sh
$ npm run dev
```



Enter directoy and run migration and seeder to insert dummy data
```sh
php artisan migrate
php artisan db:seed
```

Generate key and optimize to clear/cache route, views, config files
```sh
$ php artisan key:generate
$ php artisan optimize
```

Finally, run `php artisan serve` and open `127.0.0.1:8000/login/` and check `users` table for generated email and use it for your login credential. ex :

```
email   : john@example.com
pass    : password
```

---
## Troubleshooting

If theres some error about regarding Laravel logs : permission denied, heres some tips :

```sh
$ sudo chgrp -R www-data storage bootstrap/cache
$ sudo chmod -R ug+rwx storage bootstrap/cache
```

---
## Phpunit test

Run
```sh
$ ./vendor/bin/phpunit/
```
Make sure the file is executable. 

---
## API Documentation

In this section will discuss about API Implementation, 
Remember every request for API will require `Authorization : Bearer {token}` you can get `{token}` by generating it from `API Token` in top right corner of dashboard.

### Get Current Employee

```http
    GET /employee/{user_id}
```

will return :

```json
{
    "user_id": 1,
    "status": "inactive",
    "position": "Roof Bolters Mining"
}
```

### Insert Employee

```http
    POST /employee
```
| Parameter | Type | Description |
| :--- | :--- | :--- |
| `user_id` | `string` | **Required**. User foreign ID |
| `status` | `enum` | **Required**. 'active' or 'inactive' |
| `position` | `string` | **Required**. Employee Position |

will return :
```json
{
    "status": "success",
    "data": "data successfuly inserted"
}
```

### Update Employee
```http
    PATCH /employee/{user_id}
```

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `user_id` | `string` | **Required**. User foreign ID |
| `status` | `enum` | **Required**. 'active' or 'inactive' |
| `position` | `string` | **Required**. Employee Position |

will return :
```json
{
    "status": "success",
    "data": "data successfuly updated"
}
```

### Delete Employee
```http
    DELETE /employee/{user_id}
```

| Parameter | Type | Description |
| :--- | :--- | :--- |

will return :
```json
{
    "status": "success",
    "data": "data successfuly deleted"
}
```