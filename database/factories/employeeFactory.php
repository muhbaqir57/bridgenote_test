<?php

namespace Database\Factories;

use App\Models\employee;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = employee::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $status     =   ['active', 'inactive'];

        return [
            "user_id"   =>  \App\Models\User::factory(),
            "status"    =>  $status[ rand(0,1) ],
            "position"  =>  $this->faker->jobTitle
        ];
    }
}
