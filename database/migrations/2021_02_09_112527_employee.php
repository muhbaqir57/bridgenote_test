<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Employee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function( Blueprint $table ){
            $table->foreignId('user_id')->constrained("users");
            $table->enum('status', ['active', 'inactive'] );
            $table->string('position');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Disable and enable foreign key constraints 
        // before dropping the table

        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('employee');
        Schema::enableForeignKeyConstraints();
    }
}
